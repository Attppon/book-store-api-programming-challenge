package com.service.api.interceptor;

import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.http.entity.ContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import com.service.api.constant.CommonConstant;
import com.service.api.constant.DateTimeConstant;
import com.service.api.dao.UserSessionDao;
import com.service.api.domain.UsersSession;
import com.service.api.model.rest.BaseRequest;
import com.service.api.model.rest.BaseResponse;
import com.service.api.service.LoggerService;
import com.service.api.util.DateUtil;
import com.service.api.util.JSONUtil;
import com.service.api.util.StringUtils;

@Component
public class Interceptor implements HandlerInterceptor {

	@Value("${server.servlet.context-path}")
	private String contextPath;

	@Autowired
	private LoggerService loggerService;
	@Autowired
	private UserSessionDao userSessionDao;

	private final String[] ignoredRequestURI = {"/version","/login","/books"};
	private final String[] ignoredMethodRequestURI = {"/users"};

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

		String exceptionCode = null;
		String sessionTimeout = null;
		String sid = request.getHeader("sid");
		String swaggerUrl = contextPath+"/swagger-ui.html";
		
		int responseStatusCode = 200;
		int defaultSessionTimeSec = 6000;

		boolean ignoredRequestFlag = false;
		boolean ignoredRequestURLFlag = false;

		Date startDate = new Date();
		BaseRequest userDetailsRequest = new BaseRequest();

		
		if (swaggerUrl.equals(request.getRequestURI())) {
			ignoredRequestURLFlag = true;
		}else{
			if (ignoredMethodRequestURI != null && ignoredMethodRequestURI.length > 0) {
				for (String uri : ignoredMethodRequestURI) {
					if (request.getRequestURI().equals(contextPath+uri)) {
						if(request.getMethod().equals("POST")){
							ignoredRequestURLFlag = true;
							break;
						}
					}
				}
			}
		}
		
		if (ignoredRequestURI != null && ignoredRequestURI.length > 0) {
			for (String uri : ignoredRequestURI) {
				if (request.getRequestURI().contains(uri)) {
					ignoredRequestFlag = true;
					break;
				}
			}
		}

		if (ignoredRequestURLFlag) {

			String requestBodyPlain = IOUtils.toString(request.getReader());
			userDetailsRequest.setDecryptData(requestBodyPlain);
			loggerService.systemLogger(sid, "Request body plain text.", userDetailsRequest.getDecryptData(), CommonConstant.LOG_LEVEL_INFO);

		}else{

			if (!ignoredRequestFlag) {

				String requestBodyPlain = IOUtils.toString(request.getReader());
				userDetailsRequest.setDecryptData(requestBodyPlain);
				loggerService.systemLogger(sid, "Request body plain text.", userDetailsRequest.getDecryptData(), CommonConstant.LOG_LEVEL_INFO);

				if (StringUtils.isNotEmptyOrNull(sid)) {

					UsersSession userSessionFind = new UsersSession();
					userSessionFind.setUssIsDelete(CommonConstant.FLAG_N);
					userSessionFind.setUssSid(sid);
					UsersSession userSessionDB = userSessionDao.find(userSessionFind);

					if (userSessionDB == null) {
						loggerService.systemLogger(sid, "sid not found.",
								CommonConstant.RESPONSE_FAIL, CommonConstant.LOG_LEVEL_INFO);

						exceptionCode = "sid not found.";
						responseStatusCode = 401;

					} else {

						Calendar calendar = Calendar.getInstance();
						calendar.setTime(userSessionDB.getUssExpiryTime());

						if (new Date().compareTo(calendar.getTime()) == 1) {
							loggerService.systemLogger(sid, "session timeout.",
									CommonConstant.RESPONSE_FAIL, CommonConstant.LOG_LEVEL_INFO);

							exceptionCode = "session timeout.";
							responseStatusCode = 403;
							userSessionDB.setUssIsDelete(CommonConstant.FLAG_Y);

						} else {
							//extend session time
							try {
								calendar.setTime(startDate);
								calendar.add(Calendar.SECOND, StringUtils.isNotEmptyOrNull(sessionTimeout)
										? Integer.parseInt(sessionTimeout) : defaultSessionTimeSec);
								userSessionDB.setUssExpiryTime(calendar.getTime());

							} catch (NumberFormatException e) {
								loggerService.systemLogger(sid, "Load sessionTimeout from database failed. default="
										+ defaultSessionTimeSec, CommonConstant.RESPONSE_FAIL, CommonConstant.LOG_LEVEL_INFO);
							}

						}

						userSessionDao.update(userSessionDB);

						loggerService.systemLogger(sid, "Extend session time success.",
								userSessionDB.toString(), CommonConstant.LOG_LEVEL_INFO);

					}
				}else{
					loggerService.systemLogger(sid, "sid is null.", CommonConstant.RESPONSE_FAIL, CommonConstant.LOG_LEVEL_INFO);

					exceptionCode = "sid is null.";
					responseStatusCode = 401;
				}

				userDetailsRequest.setSid(sid);

			}
		}

		if (StringUtils.isNotEmptyOrNull(exceptionCode)) {

			response.setStatus(responseStatusCode);
			response.setContentType(ContentType.APPLICATION_JSON.toString());

			BaseResponse responseError = new BaseResponse();
			responseError.setStatus(CommonConstant.RESPONSE_FAIL);
			responseError.setTimestamp(DateUtil.formatDatetoString(new Date(),
					DateTimeConstant.APPLICATION_DATE_TIME_GLOBAL, null));
			responseError.setError(exceptionCode);
			responseError.setMessage("");

			PrintWriter out = response.getWriter();
			String responseOut = JSONUtil.transformObjectToString(responseError);
			out.print(responseOut);

			loggerService.accessLogger(startDate, new Date(), sid, "Session validate failed.", null,
					responseOut, CommonConstant.LOG_LEVEL_INFO);

			return false;

		} else {
			request.setAttribute(CommonConstant.APPLICATION_DATA_DETAILS, userDetailsRequest);
			loggerService.systemLogger(sid, "Add data details.",
					userDetailsRequest.toString(), CommonConstant.LOG_LEVEL_INFO);

		}

		return true;
	}
}
