package com.service.api.exceptionHandler;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.service.api.constant.CommonConstant;
import com.service.api.constant.ExceptionConstant;
import com.service.api.exceptions.DatabaseException;
import com.service.api.exceptions.ServiceException;
import com.service.api.exceptions.ServiceValidation;
import com.service.api.model.rest.BaseRequest;
import com.service.api.model.rest.BaseResponse;
import com.service.api.service.LoggerService;
import com.service.api.util.DateUtil;
import com.service.api.util.StringUtils;

@ControllerAdvice
public class GlobalExceptionHandler {

    @Autowired
    private LoggerService logger;

    private BaseResponse generateResponseMessage(Exception ex, String messageError, BaseRequest requestHeader) {
    	
        BaseResponse response = new BaseResponse();
        String exceptionRefID = null;
        String exceptionCode;
        String exceptionMessage;

        String messageCode = ex.getMessage();
        if (StringUtils.isNotEmptyOrNull(messageError)) {
            exceptionCode = messageError;
        } else {
            exceptionCode = messageCode;
        }

        if (requestHeader != null) {
            exceptionRefID = requestHeader.getSid();
        }

        exceptionMessage = exceptionCode != null && exceptionCode.length() > 50 ? "" : exceptionCode;
        
        response.setStatus(CommonConstant.RESPONSE_FAIL);
        response.setStatusCode(exceptionCode);
        response.setMessage(exceptionMessage);
        response.setTimestamp(DateUtil.formatDatetoString(new Date(), DateUtil.DATE_TIME, CommonConstant.LANGUAGE_TH));

        logger.printStackTraceToErrorLog(exceptionRefID, ex.getClass().getSimpleName(), ex);
        return response;
    }

    @ExceptionHandler(DatabaseException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public BaseResponse handleDatabaseException(DatabaseException ex,
            @RequestAttribute(value = CommonConstant.APPLICATION_DATA_DETAILS) BaseRequest requestHeader) {
        return generateResponseMessage(ex, null, requestHeader);
    }

    @ExceptionHandler(DataAccessException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public BaseResponse handleDataAccessException(DataAccessException ex,
            @RequestAttribute(value = CommonConstant.APPLICATION_DATA_DETAILS) BaseRequest requestHeader) {
        return generateResponseMessage(ex, null, requestHeader);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public BaseResponse handleMethodArgumentNotValidException(MethodArgumentNotValidException ex,
            @RequestAttribute(value = CommonConstant.APPLICATION_DATA_DETAILS) BaseRequest requestHeader) {
        String defaultMessage = ex.getBindingResult().getAllErrors().get(0).getDefaultMessage();
        return generateResponseMessage(ex, defaultMessage, requestHeader);
    }

    @ExceptionHandler(ServiceException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public BaseResponse handleServiceException(ServiceException ex,
            @RequestAttribute(value = CommonConstant.APPLICATION_DATA_DETAILS) BaseRequest requestHeader) {
        return generateResponseMessage(ex, null, requestHeader);
    }

    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    public BaseResponse accessDeniedException(Exception ex,
            @RequestAttribute(value = CommonConstant.APPLICATION_DATA_DETAILS) BaseRequest requestHeader) {
        return generateResponseMessage(ex, ExceptionConstant.ERROR_CODE_NOT_ALLOW_TO_ACCESS, requestHeader);
    }
    
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public BaseResponse handleException(Exception ex,
            @RequestAttribute(value = CommonConstant.APPLICATION_DATA_DETAILS) BaseRequest requestHeader) {
        String defaultMessage = ex.getCause().getCause().getMessage();
        return generateResponseMessage(ex, defaultMessage, requestHeader);
    }


    @ExceptionHandler(ServiceValidation.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public BaseResponse handleServiceValidation(ServiceValidation ex,
            @RequestAttribute(value = CommonConstant.APPLICATION_DATA_DETAILS) BaseRequest requestHeader) {
        return generateResponseMessage(ex, null, requestHeader);
    }

}
