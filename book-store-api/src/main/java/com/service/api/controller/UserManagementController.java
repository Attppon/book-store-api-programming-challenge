package com.service.api.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.service.api.constant.CommonConstant;
import com.service.api.constant.ServiceNameConstant;
import com.service.api.model.rest.BaseRequest;
import com.service.api.model.rest.BaseResponse;
import com.service.api.model.rest.request.OrderRequest;
import com.service.api.model.rest.request.UserRequest;
import com.service.api.model.rest.response.OrderResponse;
import com.service.api.model.rest.response.UserResponse;
import com.service.api.service.LoggerService;
import com.service.api.service.UserManagementService;
import com.service.api.util.JSONUtil;

@RestController
public class UserManagementController extends UserManagementControllerValidator{

	@Autowired
	private LoggerService loggerService;
	@Autowired
	private UserManagementService userManagementService;
	
	
	@GetMapping(ServiceNameConstant.SERVICE_USERS)
	@ResponseBody
	public Object getUser(@RequestAttribute(value = CommonConstant.APPLICATION_DATA_DETAILS) BaseRequest requestHeader) throws Exception {
		
		Date startDate = new Date();
        UserResponse response = null;

        response = userManagementService.getUser(requestHeader);

        loggerService.accessLogger(startDate, new Date(), requestHeader.getSid(),
        		ServiceNameConstant.SERVICE_USERS + CommonConstant.LABEL_END , null, response, CommonConstant.LOG_LEVEL_INFO);
        
        return response;
        
	}
	
	
	@PostMapping(ServiceNameConstant.SERVICE_USERS)
	@ResponseBody
	public Object addUser(@RequestAttribute(value = CommonConstant.APPLICATION_DATA_DETAILS) BaseRequest requestHeader) throws Exception {
		
		Date startDate = new Date();
		BaseResponse response = null;
     
        UserRequest request = (UserRequest) JSONUtil.transformStringToObject(requestHeader.getDecryptData(), UserRequest.class);

        validationAddUser(request);
        
        response = userManagementService.addUser(request);

        loggerService.accessLogger(startDate, new Date(), requestHeader.getSid(),
        		ServiceNameConstant.SERVICE_USERS + CommonConstant.LABEL_END , null, response, CommonConstant.LOG_LEVEL_INFO);
        
        return response;
        
	}
	
	@DeleteMapping(ServiceNameConstant.SERVICE_USERS)
	@ResponseBody
	public Object deleteUserHistory(@RequestAttribute(value = CommonConstant.APPLICATION_DATA_DETAILS) BaseRequest requestHeader) throws Exception {
		
		Date startDate = new Date();
		BaseResponse response = null;
     
        response = userManagementService.deleteUserHistory(requestHeader);

        loggerService.accessLogger(startDate, new Date(), requestHeader.getSid(),
        		ServiceNameConstant.SERVICE_USERS + CommonConstant.LABEL_END , null, response, CommonConstant.LOG_LEVEL_INFO);
        
        return response;
        
	}
	
	@PostMapping(ServiceNameConstant.SERVICE_USERS_ORDER)
	@ResponseBody
	public Object userOrder(@RequestAttribute(value = CommonConstant.APPLICATION_DATA_DETAILS) BaseRequest requestHeader) throws Exception {
		
		Date startDate = new Date();
		OrderResponse response = null;
     
		OrderRequest request = (OrderRequest) JSONUtil.transformStringToObject(requestHeader.getDecryptData(), OrderRequest.class);

        validationUserOrder(request);
        
        response = userManagementService.userOrder(requestHeader, request);

        loggerService.accessLogger(startDate, new Date(), requestHeader.getSid(),
        		ServiceNameConstant.SERVICE_USERS_ORDER + CommonConstant.LABEL_END , null, response, CommonConstant.LOG_LEVEL_INFO);
        
        return response;
        
	}

}
