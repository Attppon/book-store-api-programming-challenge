package com.service.api.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.service.api.constant.CommonConstant;
import com.service.api.constant.ServiceNameConstant;
import com.service.api.model.rest.BaseRequest;
import com.service.api.model.rest.response.BooksResponse;
import com.service.api.service.ExternalService;
import com.service.api.service.LoggerService;

@RestController
public class BooksManagementController extends UserManagementControllerValidator{

	@Autowired
	private LoggerService loggerService;
	@Autowired
	private ExternalService externalService;
	
	
	@GetMapping(ServiceNameConstant.SERVICE_BOOKS)
	@ResponseBody
	public Object getBooks(@RequestAttribute(value = CommonConstant.APPLICATION_DATA_DETAILS) BaseRequest requestHeader) throws Exception {
		
		Date startDate = new Date();
		BooksResponse response = null;

        response = externalService.getBooks(requestHeader);
        
        loggerService.accessLogger(startDate, new Date(), requestHeader.getSid(),
        		ServiceNameConstant.SERVICE_USERS + CommonConstant.LABEL_END , null, response, CommonConstant.LOG_LEVEL_INFO);
        
        return response;
        
	}

}
