package com.service.api.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.service.api.constant.CommonConstant;
import com.service.api.constant.ServiceNameConstant;
import com.service.api.model.rest.request.LoginRequest;
import com.service.api.model.rest.response.LoginResponse;
import com.service.api.service.AuthenticationService;
import com.service.api.service.LoggerService;

@RestController
public class AuthenticationController extends AuthenticationControllerValidator {


    @Autowired
    private LoggerService loggerService;
    @Autowired
    private AuthenticationService authenticationService;
    
    @PostMapping(ServiceNameConstant.SERVICE_LOG_IN)
    @ResponseBody
    public Object login(@RequestBody LoginRequest request, HttpServletRequest httpRequest) throws Exception {

    	Date startDate = new Date();
    	LoginResponse response = null;

    	loginValidation(request.getUsername(), request.getPassword());

    	response = authenticationService.login(request);

    	loggerService.accessLogger(startDate, new Date(), request.getUsername(),
    			ServiceNameConstant.SERVICE_LOG_IN + CommonConstant.LABEL_END, null, response, CommonConstant.LOG_LEVEL_INFO);

    	return response;

    }
    
}