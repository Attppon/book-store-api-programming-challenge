package com.service.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import com.service.api.constant.CommonConstant;
import com.service.api.exceptions.ServiceValidation;
import com.service.api.model.rest.request.OrderRequest;
import com.service.api.model.rest.request.UserRequest;
import com.service.api.util.DateUtil;
import com.service.api.util.ObjectValidatorUtils;
import com.service.api.util.StringUtils;

public class UserManagementControllerValidator {

    @Autowired
    private ObjectValidatorUtils validator;

    protected void validationAddUser(UserRequest request) throws ServiceValidation {
    	
    	if (!validator.validateMandatory(request.getUsername())) {
    		throw new ServiceValidation("username is required");

    	}
    	
    	if (!validator.validateMandatory(request.getPassword())) {
    		throw new ServiceValidation("password is required");

    	}
    	
    	if (StringUtils.isNotEmptyOrNull(request.getDate_of_birth())) {
    		if(!DateUtil.validateDate(request.getDate_of_birth(), DateUtil.DEFAULT_DATE_FORMAT, CommonConstant.LANGUAGE_EN)){
    			throw new ServiceValidation("date_of_birth is valid");
    		}
    	}
    	    	       
   }

    protected void validationUserOrder(OrderRequest request) throws ServiceValidation {

    	if (CollectionUtils.isEmpty(request.getOrders())) {
    		throw new ServiceValidation("orders is required");

    	}
    }

}
