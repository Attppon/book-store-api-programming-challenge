package com.service.api.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.service.api.constant.ServiceNameConstant;

@RestController
public class ApplicationController {

	@Value("${bulid.name}")
    private String appName;

    @Value("${bulid.version}")
    private String appVersion;

    @GetMapping(ServiceNameConstant.SERVICE_VERSION)
    @ResponseBody
    public String getVersion() throws Exception {
    	
        StringBuilder response = new StringBuilder();
        response.append("[ApplicationName : ").append(appName).append(", Version : ").append(appVersion).append("]");
        
        return response.toString();
    }
}
