package com.service.api.constant;

public class CommonConstant {

    private CommonConstant() {
        throw new IllegalStateException();
    }

    public static final String APPLICATION_SYSTEM = "SYSTEM";
    public static final String APPLICATION_DATA_DETAILS = "DATA_DETAILS";
    public static final String UTF_8 = "UTF-8";
    public static final String LANGUAGE_TH = "TH";
    public static final String LANGUAGE_EN = "EN";
    public static final String LANGUAGE = "language";
    public static final String APPLICATION_LANGUAGE_REGEX = "^TH|EN|th|en";
    public static final String APPLICATION_NUMBER_REGEX = "^[0-9]*$";
    public static final String RESPONSE_SUCCESS = "success";
    public static final String RESPONSE_FAIL = "fail";
    public static final String RESPONSE_CODE_SUCCESS = "000000";
    public static final String RESPONSE_MESSAGE_SUCCESS_EN = "Transaction Success.";
    public static final String FLAG_Y = "Y";
    public static final String FLAG_N = "N";
    public static final String FLAG_A = "A";
    public static final String LABEL_END = " end.";
    public static final String ACCESS_LOG = "[ACCESS]";
    public static final String SYSTEM_LOG = "[SYSTEM]";
    public static final String ERROR_LOG = "[ERROR]";
    public static final String EXTERNAL_LOG = "[EXTERNAL]";
    public static final String LOG_LEVEL_TRACE = "TRACE";
    public static final String LOG_LEVEL_DEBUG = "DEBUG";
    public static final String LOG_LEVEL_INFO = "INFO";
    public static final String LOG_LEVEL_WARN = "WARN";
    public static final String LOG_LEVEL_ERROR = "ERROR";
    public static final String LOG_LEVEL_FATAL = "FATAL";
    public static final String LOG_EXCEPTION = "EXCEPTION";
    public static final String LOG_SERVICE_EXCEPTION = "SERVICE_EXCEPTION";
    public static final String LOG_SERVICE_VALIDATION = "SERVICE_VALIDATION";
    public static final String LOG_DATABASE_EXCEPTION = "DATABASE_EXCEPTION";
    public static final String LOG_DATABASE_ACCESS_SQL_EXCEPTION = "DATABASE_ACCESS_SQL_EXCEPTION";

}
