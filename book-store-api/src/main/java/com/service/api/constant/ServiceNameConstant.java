package com.service.api.constant;

public class ServiceNameConstant {

    private ServiceNameConstant() {
        throw new IllegalStateException();
    }

    
    public static final String BASE_URL_BOOK = "https://scb-test-book-publisher.herokuapp.com/books";
    public static final String URL_BOOK_RECOMMENDATION = "/recommendation";
    public static final String SERVICE_VERSION = "/version";
    public static final String SERVICE_LOG_IN = "/login";
    public static final String SERVICE_BOOKS = "/books";
    public static final String SERVICE_USERS = "/users";
    public static final String SERVICE_USERS_ORDER = "/users/orders";

}
