package com.service.api.constant;

public class DateTimeConstant {

    private DateTimeConstant() {
        throw new IllegalStateException();
    }

    public static final String APPLICATION_DATE_TIME_GLOBAL = "yyyy-MM-dd HH:mm:ss";
    public static final String APPLICATION_DATE_TIME_GLOBAL_REGEX = "[0-9]{4}\\/(0[1-9]|1[0-2])\\/(0[1-9]|[1-2][0-9]|3[0-1])";

    public static final String DATE_TIME_FORMAT_SLASH = "yyyy/MM/dd HH:mm:ss";
    public static final String DATE_TIME_FORMAT_SLASH_REGEX = "[0-9]{4}\\/(0[1-9]|1[0-2])\\/(0[1-9]|[1-2][0-9]|3[0-1]) (2[0-3]|[01][0-9]):[0-5][0-9]";

    public static final String DATE_TIME_FORMAT_DASH = "yyyy-MM-dd HH:mm:ss";

    public static final String DATE_FORMAT_HYPHEN = "yyyy-MM-dd";
    public static final String DATE_FORMAT_SLASH = "yyyy/MM/dd";
    public static final String DATE_FORMAT = "ddMMyyyy";
    public static final String AWS_DATE_TIME_FORMAT_YYYYMMMDDD = "yyyyMMdd";
    public static final String DATE_TIME_FORMAT = "yyyyMMddHHmmss";
    public static final String TIME_FORMAT = "HH:mm:ss";

    public static final String DATE_FORMAT_BOS_SLASH = "dd/MM/yyyy";
    public static final String DATE_TIME_FORMAT_BOS_SLASH = "dd/MM/yyyy HH:mm:ss";
    public static final String DATE_TIME_MILLISEC_FORMAT_BOS_SLASH = "dd/MM/yyyy HH:mm:ss.SSS";
    public static final String DATE_TIME_FORMAT_REPORT_SLASH = "dd/MM/yyyy HH:mm";

    public static final String START_DATE = " 00:00:00";
    public static final String END_DATE = " 23:59:59";
    public static final String DEFAULT_DATE_FORMAT = "dd/MM/yyyy";

    public static final String DEFAULT_DATETIME_FORMAT7 = "yyyyMMddHHmmssSSS";

}
