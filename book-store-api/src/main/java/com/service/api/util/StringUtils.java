package com.service.api.util;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;

public class StringUtils {

    private StringUtils() {
        throw new IllegalStateException("Utility class");
    }

    public static boolean isNotEmptyOrNull(String str) {
        return !(null == str || "".equals(str.trim()) || str.trim().length() == 0 || "null".equalsIgnoreCase(str));
    }

    public static boolean isEmptyOrNull(String str) {
        return (str == null || "".equals(str.trim()) || str.trim().length() == 0);
    }

    public static String toString(String str) {
        return (str == null ? "" : str.trim());
    }
    
    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
    	Map<Object, Boolean> seen = new ConcurrentHashMap<>(); 
    	return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null; 
    }
   
}
