package com.service.api.domain;

import java.sql.Timestamp;

public class Orders {
	   
	private Long odId;           
	private Long odUsrId;       
	private Long odBookId;      
	private String odBookName;    
	private Double odBookPrice;   
	private Timestamp odCreateDate;  
	private String odCreator;      
	private Long odCreatorId;   
	private Timestamp odUpdateDate;  
	private String odUpdater;      
	private Long odUpdaterId;
	
	
	
	public Orders() {
		super();
	}
	public Orders(Long odId, Long odUsrId, Long odBookId, String odBookName, Double odBookPrice) {
		this.odId = odId;
		this.odUsrId = odUsrId;
		this.odBookId = odBookId;
		this.odBookName = odBookName;
		this.odBookPrice = odBookPrice;
	}
	public Long getOdId() {
		return odId;
	}
	public void setOdId(Long odId) {
		this.odId = odId;
	}
	public Long getOdUsrId() {
		return odUsrId;
	}
	public void setOdUsrId(Long odUsrId) {
		this.odUsrId = odUsrId;
	}
	public Long getOdBookId() {
		return odBookId;
	}
	public void setOdBookId(Long odBookId) {
		this.odBookId = odBookId;
	}
	public String getOdBookName() {
		return odBookName;
	}
	public void setOdBookName(String odBookName) {
		this.odBookName = odBookName;
	}
	public Double getOdBookPrice() {
		return odBookPrice;
	}
	public void setOdBookPrice(Double odBookPrice) {
		this.odBookPrice = odBookPrice;
	}
	public Timestamp getOdCreateDate() {
		return odCreateDate;
	}
	public void setOdCreateDate(Timestamp odCreateDate) {
		this.odCreateDate = odCreateDate;
	}
	public String getOdCreator() {
		return odCreator;
	}
	public void setOdCreator(String odCreator) {
		this.odCreator = odCreator;
	}
	public Long getOdCreatorId() {
		return odCreatorId;
	}
	public void setOdCreatorId(Long odCreatorId) {
		this.odCreatorId = odCreatorId;
	}
	public Timestamp getOdUpdateDate() {
		return odUpdateDate;
	}
	public void setOdUpdateDate(Timestamp odUpdateDate) {
		this.odUpdateDate = odUpdateDate;
	}
	public String getOdUpdater() {
		return odUpdater;
	}
	public void setOdUpdater(String odUpdater) {
		this.odUpdater = odUpdater;
	}
	public Long getOdUpdaterId() {
		return odUpdaterId;
	}
	public void setOdUpdaterId(Long odUpdaterId) {
		this.odUpdaterId = odUpdaterId;
	}
}
