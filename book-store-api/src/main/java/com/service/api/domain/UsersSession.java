package com.service.api.domain;

import java.sql.Timestamp;
import java.util.Date;

public class UsersSession {

	private String ussSid;           
	private Long ussUsrId;         
	private String ussUsrUsername;      
	private Date ussLoginDateTime;
	private Date ussLogoutDateTime;     
	private Date ussExpiryTime;     
	private String ussIsDelete;
	private Timestamp ussCreateDate;  
	private Long ussCreatorId;   
	private Timestamp ussUpdateDate;  
	private Long ussUpdaterId;
	
	public String getUssSid() {
		return ussSid;
	}
	public void setUssSid(String ussSid) {
		this.ussSid = ussSid;
	}
	public Long getUssUsrId() {
		return ussUsrId;
	}
	public void setUssUsrId(Long ussUsrId) {
		this.ussUsrId = ussUsrId;
	}
	public String getUssUsrUsername() {
		return ussUsrUsername;
	}
	public void setUssUsrUsername(String ussUsrUsername) {
		this.ussUsrUsername = ussUsrUsername;
	}
	public Date getUssLoginDateTime() {
		return ussLoginDateTime;
	}
	public void setUssLoginDateTime(Date ussLoginDateTime) {
		this.ussLoginDateTime = ussLoginDateTime;
	}
	public Date getUssLogoutDateTime() {
		return ussLogoutDateTime;
	}
	public void setUssLogoutDateTime(Date ussLogoutDateTime) {
		this.ussLogoutDateTime = ussLogoutDateTime;
	}
	public Date getUssExpiryTime() {
		return ussExpiryTime;
	}
	public void setUssExpiryTime(Date ussExpiryTime) {
		this.ussExpiryTime = ussExpiryTime;
	}
	public String getUssIsDelete() {
		return ussIsDelete;
	}
	public void setUssIsDelete(String ussIsDelete) {
		this.ussIsDelete = ussIsDelete;
	}
	public Timestamp getUssCreateDate() {
		return ussCreateDate;
	}
	public void setUssCreateDate(Timestamp ussCreateDate) {
		this.ussCreateDate = ussCreateDate;
	}
	public Long getUssCreatorId() {
		return ussCreatorId;
	}
	public void setUssCreatorId(Long ussCreatorId) {
		this.ussCreatorId = ussCreatorId;
	}
	public Timestamp getUssUpdateDate() {
		return ussUpdateDate;
	}
	public void setUssUpdateDate(Timestamp ussUpdateDate) {
		this.ussUpdateDate = ussUpdateDate;
	}
	public Long getUssUpdaterId() {
		return ussUpdaterId;
	}
	public void setUssUpdaterId(Long ussUpdaterId) {
		this.ussUpdaterId = ussUpdaterId;
	}
	
}
