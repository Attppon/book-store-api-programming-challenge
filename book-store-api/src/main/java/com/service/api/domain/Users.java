package com.service.api.domain;

import java.sql.Date;
import java.sql.Timestamp;

public class Users {

	private Long usrId;           
	private String usrName;         
	private String usrSurname;      
	private Date usrDateOfBirth;
	private String usrUsername;     
	private String usrPassword;     
	private String usrStatus;       
	private Timestamp usrCreateDate;  
	private String usrCreator;      
	private Long usrCreatorId;   
	private Timestamp usrUpdateDate;  
	private String usrUpdater;      
	private Long usrUpdaterId;
	
	public Users() {
		super();
	}
	
	public Users(String usrUsername, String usrPassword) {
		this.usrUsername = usrUsername;
		this.usrPassword = usrPassword;
	}
	
	public Long getUsrId() {
		return usrId;
	}
	public void setUsrId(Long usrId) {
		this.usrId = usrId;
	}
	public String getUsrName() {
		return usrName;
	}
	public void setUsrName(String usrName) {
		this.usrName = usrName;
	}
	public String getUsrSurname() {
		return usrSurname;
	}
	public void setUsrSurname(String usrSurname) {
		this.usrSurname = usrSurname;
	}
	public Date getUsrDateOfBirth() {
		return usrDateOfBirth;
	}
	public void setUsrDateOfBirth(Date usrDateOfBirth) {
		this.usrDateOfBirth = usrDateOfBirth;
	}
	public String getUsrUsername() {
		return usrUsername;
	}
	public void setUsrUsername(String usrUsername) {
		this.usrUsername = usrUsername;
	}
	public String getUsrPassword() {
		return usrPassword;
	}
	public void setUsrPassword(String usrPassword) {
		this.usrPassword = usrPassword;
	}
	public String getUsrStatus() {
		return usrStatus;
	}
	public void setUsrStatus(String usrStatus) {
		this.usrStatus = usrStatus;
	}
	public Timestamp getUsrCreateDate() {
		return usrCreateDate;
	}
	public void setUsrCreateDate(Timestamp usrCreateDate) {
		this.usrCreateDate = usrCreateDate;
	}
	public String getUsrCreator() {
		return usrCreator;
	}
	public void setUsrCreator(String usrCreator) {
		this.usrCreator = usrCreator;
	}
	public Long getUsrCreatorId() {
		return usrCreatorId;
	}
	public void setUsrCreatorId(Long usrCreatorId) {
		this.usrCreatorId = usrCreatorId;
	}
	public Timestamp getUsrUpdateDate() {
		return usrUpdateDate;
	}
	public void setUsrUpdateDate(Timestamp usrUpdateDate) {
		this.usrUpdateDate = usrUpdateDate;
	}
	public String getUsrUpdater() {
		return usrUpdater;
	}
	public void setUsrUpdater(String usrUpdater) {
		this.usrUpdater = usrUpdater;
	}
	public Long getUsrUpdaterId() {
		return usrUpdaterId;
	}
	public void setUsrUpdaterId(Long usrUpdaterId) {
		this.usrUpdaterId = usrUpdaterId;
	}
	
}
