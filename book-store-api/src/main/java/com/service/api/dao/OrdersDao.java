package com.service.api.dao;

import java.util.List;

import com.service.api.domain.Orders;

public interface OrdersDao {

	public Long insert(Orders obj) throws Exception;
	
	public int deleteByUserId(Long userId) throws Exception;
	
	public List<Orders> findByUserId(Long usrId) throws Exception;

}
