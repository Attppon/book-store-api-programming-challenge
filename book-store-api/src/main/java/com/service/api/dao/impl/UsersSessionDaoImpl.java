package com.service.api.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.service.api.constant.CommonConstant;
import com.service.api.dao.UserSessionDao;
import com.service.api.domain.UsersSession;
import com.service.api.exceptions.DatabaseException;
import com.service.api.service.LoggerService;
import com.service.api.util.DateUtil;

@Repository
public class UsersSessionDaoImpl implements UserSessionDao {

	@Autowired
	private LoggerService loggerService;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public UsersSession find(UsersSession obj) throws Exception {
		
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT *  ");
        sql.append(" FROM USERS_SESSION   ");
        sql.append(" WHERE USS_SID = ?  ");

        List<UsersSession> listResult = null;

        try {
            listResult = (List<UsersSession>) jdbcTemplate.query(sql.toString(), new Object[]{obj.getUssSid()}, new BeanPropertyRowMapper<UsersSession>(UsersSession.class));
        } catch (Exception e) {
        	loggerService.printStackTraceToErrorLog(null, CommonConstant.LOG_DATABASE_EXCEPTION, e);
            throw new DatabaseException(e);
        }

        return listResult != null && listResult.size() > 0 ? listResult.get(0) : null;

    }

	@Override
	public int insert(UsersSession obj) throws Exception {
		
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO USERS_SESSION (");
        sql.append(" USS_CREATE_DATE ");
        sql.append(",USS_SID ");
        sql.append(",USS_USR_ID ");
        sql.append(",USS_USR_USERNAME ");
        sql.append(",USS_LOGIN_DATE_TIME ");
        sql.append(",USS_LOGOUT_DATE_TIME ");
        sql.append(",USS_EXPIRY_TIME ");
        sql.append(",USS_IS_DELETE ");
        sql.append(",USS_CREATOR_ID ");
        sql.append(") VALUES ( ");
        sql.append("sysdate(),");
        sql.append("?,");
        sql.append("?,");
        sql.append("?,");
        sql.append("?,");
        sql.append("?,");
        sql.append("?,");
        sql.append("?,");
        sql.append("?");
        sql.append(") ");
        
        int row = jdbcTemplate.update(sql.toString(), new PreparedStatementSetter() {
            int i = 1;

            @Override
            public void setValues(PreparedStatement ps) throws SQLException {
            	
            	 ps.setString(i++, obj.getUssSid());
            	 
            	 if (obj.getUssUsrId() != null) {
                     ps.setLong(i++, obj.getUssUsrId());
                 }else{
                	  ps.setNull(i++, Types.NULL);
                 }
            	 
            	 ps.setString(i++, obj.getUssUsrUsername());
            	 
            	 if (obj.getUssLoginDateTime() != null) {
                     ps.setTimestamp(i++, DateUtil.toTimeStamp(obj.getUssLoginDateTime()));
                 } else {
                     ps.setNull(i++, Types.NULL);
                 }
            	 
            	 if (obj.getUssLogoutDateTime() != null) {
                     ps.setTimestamp(i++, DateUtil.toTimeStamp(obj.getUssLogoutDateTime()));
                 } else {
                     ps.setNull(i++, Types.NULL);
                 }
            	 
            	 if (obj.getUssExpiryTime() != null) {
                     ps.setTimestamp(i++, DateUtil.toTimeStamp(obj.getUssExpiryTime()));
                 } else {
                     ps.setNull(i++, Types.NULL);
                 }
            	 
            	 ps.setString(i++, obj.getUssIsDelete());
            	 
                if (obj.getUssCreatorId() != null) {
                    ps.setLong(i++, obj.getUssCreatorId());
                } else {
                    ps.setNull(i++, Types.NULL);
                }
            }
        });

        return row;
    }

	@Override
	public int update(UsersSession obj) throws Exception {
        StringBuilder sql = new StringBuilder();
        int row = 0;
        
        try {
        	
            sql.append("UPDATE ");
            sql.append("USERS_SESSION ");
            sql.append("SET USS_UPDATE_DATE = sysdate() ");
            sql.append(",USS_USR_ID = ? ");
            sql.append(",USS_USR_USERNAME = ? ");
            sql.append(",USS_LOGIN_DATE_TIME = ? ");
            sql.append(",USS_LOGOUT_DATE_TIME = ? ");
            sql.append(",USS_EXPIRY_TIME = ? ");
            sql.append(",USS_IS_DELETE = ? ");
            sql.append(",USS_UPDATER_ID = ? ");
            sql.append("WHERE ");
            sql.append("USS_SID = ? ");

            row = jdbcTemplate.update(sql.toString(), new PreparedStatementSetter() {
                int i = 1;

                @Override
                public void setValues(PreparedStatement ps) throws SQLException {

                	 if (obj.getUssUsrId() != null) {
                         ps.setLong(i++, obj.getUssUsrId());
                     }else{
                    	  ps.setNull(i++, Types.NULL);
                     }
                	 
                	 ps.setString(i++, obj.getUssUsrUsername());
                	 
                	 if (obj.getUssLoginDateTime() != null) {
                         ps.setTimestamp(i++, DateUtil.toTimeStamp(obj.getUssLoginDateTime()));
                     } else {
                         ps.setNull(i++, Types.NULL);
                     }
                	 
                	 if (obj.getUssLogoutDateTime() != null) {
                         ps.setTimestamp(i++, DateUtil.toTimeStamp(obj.getUssLogoutDateTime()));
                     } else {
                         ps.setNull(i++, Types.NULL);
                     }
                	 
                	 if (obj.getUssExpiryTime() != null) {
                         ps.setTimestamp(i++, DateUtil.toTimeStamp(obj.getUssExpiryTime()));
                     } else {
                         ps.setNull(i++, Types.NULL);
                     }
                	 
                	 ps.setString(i++, obj.getUssIsDelete());
                	 
                    if (obj.getUssUpdaterId() != null) {
                        ps.setLong(i++, obj.getUssUpdaterId());
                    } else {
                        ps.setNull(i++, Types.NULL);
                    }

                    ps.setString(i++, obj.getUssSid());
                }

            });
        } catch (Exception e) {
        	loggerService.printStackTraceToErrorLog(null, CommonConstant.LOG_DATABASE_EXCEPTION, e);
            throw new DatabaseException(e);
        }
        
        return row;
    }

	
	

}
