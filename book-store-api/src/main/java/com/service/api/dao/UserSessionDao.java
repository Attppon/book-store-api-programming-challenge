package com.service.api.dao;

import com.service.api.domain.UsersSession;

public interface UserSessionDao {

	public UsersSession find(UsersSession obj) throws Exception;
	
    public int insert(UsersSession obj) throws Exception;

    public int update(UsersSession obj) throws Exception;
	
}
