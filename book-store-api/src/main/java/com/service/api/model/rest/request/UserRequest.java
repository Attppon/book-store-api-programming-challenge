package com.service.api.model.rest.request;

import com.service.api.model.rest.BaseRequest;

public class UserRequest extends BaseRequest {

	private String username;
	private String password;
	private String name;
    private String surname;
    private String date_of_birth;
    
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getDate_of_birth() {
		return date_of_birth;
	}
	public void setDate_of_birth(String date_of_birth) {
		this.date_of_birth = date_of_birth;
	}
	
	@Override
	public String toString() {
		return "UserRequest [username=" + username + ", password=" + password + ", name=" + name + ", surname="
				+ surname + ", date_of_birth=" + date_of_birth + "]";
	}
	
}
