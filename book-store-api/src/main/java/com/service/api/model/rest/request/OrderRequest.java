package com.service.api.model.rest.request;

import java.util.List;

import com.service.api.model.rest.BaseRequest;

public class OrderRequest extends BaseRequest {

	private List<Long> orders;

	public List<Long> getOrders() {
		return orders;
	}

	public void setOrders(List<Long> orders) {
		this.orders = orders;
	}

	@Override
	public String toString() {
		return "OrderRequest [orders=" + orders + "]";
	}
	
}
