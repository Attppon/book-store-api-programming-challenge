package com.service.api.model.rest.response;

import java.util.List;

import com.service.api.model.rest.BaseResponse;
import com.service.api.vo.Books;

public class BooksResponse extends BaseResponse {

	private List<Books> books;

	public List<Books> getBooks() {
		return books;
	}

	public void setBooks(List<Books> books) {
		this.books = books;
	}

	
}
