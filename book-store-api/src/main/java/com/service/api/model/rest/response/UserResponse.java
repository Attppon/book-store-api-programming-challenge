package com.service.api.model.rest.response;

import java.util.List;

import com.service.api.model.rest.BaseResponse;

public class UserResponse extends BaseResponse {

    private String name;
    private String surname;
    private String date_of_birth;
    private List<Long> books;
    
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getDate_of_birth() {
		return date_of_birth;
	}
	public void setDate_of_birth(String date_of_birth) {
		this.date_of_birth = date_of_birth;
	}
	public List<Long> getBooks() {
		return books;
	}
	public void setBooks(List<Long> books) {
		this.books = books;
	}
	@Override
	public String toString() {
		return "UserResponse [name=" + name + ", surname=" + surname + ", date_of_birth=" + date_of_birth + ", books="
				+ books + "]";
	}
	
	
}
