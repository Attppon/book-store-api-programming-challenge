package com.service.api.model.rest;

public class BaseEncryptResponse {

    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("BaseEncryptResponse{data=").append(data);
        sb.append('}');
        return sb.toString();
    }

}
