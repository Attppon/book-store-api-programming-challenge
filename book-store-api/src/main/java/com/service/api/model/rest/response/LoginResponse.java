package com.service.api.model.rest.response;

import com.service.api.model.rest.BaseResponse;

public class LoginResponse extends BaseResponse {

    private String sid;

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }


	@Override
	public String toString() {
		return "LoginResponse{" +
				"sid='" + sid + '\'' +
				'}';
	}
}
