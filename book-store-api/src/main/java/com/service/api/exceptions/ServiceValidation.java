package com.service.api.exceptions;

public class ServiceValidation extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ServiceValidation() {

    }

    public ServiceValidation(String messageCode) {
        super(messageCode);
    }

    public ServiceValidation(String messageCode, Throwable cause) {
        super(messageCode, cause);
    }

    public ServiceValidation(Throwable cause) {
        super(cause);
    }

    public ServiceValidation(String messageCode, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(messageCode, cause, enableSuppression, writableStackTrace);
    }

}
