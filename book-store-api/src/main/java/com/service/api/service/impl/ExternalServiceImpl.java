package com.service.api.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.service.api.constant.CommonConstant;
import com.service.api.constant.DateTimeConstant;
import com.service.api.constant.ExceptionConstant;
import com.service.api.constant.ServiceNameConstant;
import com.service.api.exceptions.ServiceException;
import com.service.api.model.rest.BaseRequest;
import com.service.api.model.rest.response.BooksResponse;
import com.service.api.service.ExternalService;
import com.service.api.service.LoggerService;
import com.service.api.util.DateUtil;
import com.service.api.util.StringUtils;
import com.service.api.vo.Books;
import com.service.api.vo.BooksVO;

@Service
public class ExternalServiceImpl implements ExternalService {

    @Autowired
    private LoggerService loggerService;

	@Override
	public BooksResponse getBooks(BaseRequest request) throws Exception {

		BooksResponse response = new BooksResponse();
		Date currentDate = new Date();

		try {
			
			response.setBooks(getBookAll());

		} catch (Exception e) {
			loggerService.printStackTraceToErrorLog(null, e.getClass().getName(), e);
			throw new ServiceException(ExceptionConstant.SERVICE_EXCEPTION);
		}

		response.setStatus(CommonConstant.RESPONSE_SUCCESS);
		response.setStatusCode(CommonConstant.RESPONSE_CODE_SUCCESS);
		response.setMessage(CommonConstant.RESPONSE_MESSAGE_SUCCESS_EN);
		response.setTimestamp(DateUtil.formatDatetoString(currentDate, DateTimeConstant.APPLICATION_DATE_TIME_GLOBAL, null));

		return response;
	}
    
	
	@Override
	public List<Books> getBookAll() throws Exception {
		
		List<Books> books = new ArrayList<>();
		
		try {

			List<BooksVO> booksAll = callBookServiceExternal(ServiceNameConstant.BASE_URL_BOOK);
			List<BooksVO> booksRecommendationList = callBookServiceExternal(ServiceNameConstant.BASE_URL_BOOK+ServiceNameConstant.URL_BOOK_RECOMMENDATION);
			
			if(!CollectionUtils.isEmpty(booksRecommendationList)){
				
				booksRecommendationList.stream().forEach(obj -> obj.setRecommended(true));
				
				booksRecommendationList = booksRecommendationList.stream()
						.sorted(Comparator.comparing(BooksVO::getBook_name))
						.collect(Collectors.toList());
			}

			
			if(!CollectionUtils.isEmpty(booksAll)){
				
				booksAll = booksAll.stream()
						.sorted(Comparator.comparing(BooksVO::getBook_name))
						.collect(Collectors.toList());
			}
			
			booksRecommendationList.addAll(booksAll);

			if(!CollectionUtils.isEmpty(booksRecommendationList)){

				booksRecommendationList = booksRecommendationList.stream() 
						.filter( StringUtils.distinctByKey(BooksVO::getId)) 
						.collect(Collectors.toList());

				for (BooksVO booksVO : booksRecommendationList) {
					
					books.add(new Books(Long.valueOf(booksVO.getId())
							, booksVO.getBook_name()
							, booksVO.getAuthor_name()
							, booksVO.getPrice()
							, booksVO.isRecommended()));
					
				}
				

			}

		} catch (Exception e) {
			loggerService.printStackTraceToErrorLog(null, e.getClass().getName(), e);
			throw new ServiceException(ExceptionConstant.SERVICE_EXCEPTION);
		}

		return books;
	}


	@Override
	public List<Books> getBookByInBookId(List<Long> ids) throws Exception {

		List<Books> books = new ArrayList<>();

		try {

			List<BooksVO> booksAll = callBookServiceExternal(ServiceNameConstant.BASE_URL_BOOK);

			if(!CollectionUtils.isEmpty(booksAll)){

				for (Long id : ids) {
					for (BooksVO booksVO : booksAll.stream().filter(obj -> obj.getId().equals(String.valueOf(id))).collect(Collectors.toList())) {

						books.add(new Books(Long.valueOf(booksVO.getId())
								, booksVO.getBook_name()
								, booksVO.getAuthor_name()
								, booksVO.getPrice()
								, booksVO.isRecommended()));
					}
				}
			}

		} catch (Exception e) {
			loggerService.printStackTraceToErrorLog(null, e.getClass().getName(), e);
			throw new ServiceException(ExceptionConstant.SERVICE_EXCEPTION);
		}

		return books;
	}
    
	
	private List<BooksVO> callBookServiceExternal(String url) throws Exception{
		
		RestTemplate restTemplate = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		
		ResponseEntity<Object[]> responseEntity = restTemplate.getForEntity(url, Object[].class);
		Object[] objects = responseEntity.getBody();

		return Arrays.stream(objects)
				.map(object -> mapper.convertValue(object, BooksVO.class))
				.collect(Collectors.toList());
		
	}

}
