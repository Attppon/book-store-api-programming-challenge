package com.service.api.service;

import com.service.api.model.rest.request.LoginRequest;
import com.service.api.model.rest.response.LoginResponse;

public interface AuthenticationService {

    public LoginResponse login(LoginRequest request) throws Exception;

}
