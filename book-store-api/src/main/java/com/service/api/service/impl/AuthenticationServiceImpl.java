package com.service.api.service.impl;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.service.api.constant.CommonConstant;
import com.service.api.constant.DateTimeConstant;
import com.service.api.constant.ExceptionConstant;
import com.service.api.dao.UserSessionDao;
import com.service.api.dao.UsersDao;
import com.service.api.domain.Users;
import com.service.api.domain.UsersSession;
import com.service.api.exceptions.DatabaseException;
import com.service.api.exceptions.ServiceException;
import com.service.api.exceptions.ServiceValidation;
import com.service.api.model.rest.request.LoginRequest;
import com.service.api.model.rest.response.LoginResponse;
import com.service.api.service.AuthenticationService;
import com.service.api.service.LoggerService;
import com.service.api.util.DateUtil;
import com.service.api.util.MessageUtil;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    @Autowired
    private LoggerService loggerService;
    @Autowired
    private UsersDao usersDao;
    @Autowired
    private UserSessionDao userSessionDao;
   

    @Override
    public LoginResponse login(LoginRequest request) throws Exception {
    	
        LoginResponse response = new LoginResponse();
        Date currentDate = new Date();

        try {

        	Users loginReq = new Users();
            loginReq.setUsrUsername(request.getUsername());
            loginReq.setUsrPassword(request.getPassword());
            
            Users userLogin = usersDao.findByUsernameAndPassword(loginReq);

            if (userLogin != null) {
            	
                setLoginDetailResponse(response, userLogin, currentDate);
            } else {
            	
                loggerService.systemLogger(null, "Not found user.", null, CommonConstant.LOG_LEVEL_INFO);
                throw new ServiceException(MessageUtil.E2036 + MessageUtil.E2063);

            }

        } catch (DataAccessException e) {
            loggerService.printStackTraceToErrorLog(null, e.getClass().getName(), e);

            throw new DatabaseException(ExceptionConstant.DATABASE_EXCEPTION);

        } catch (ServiceValidation e) {
            loggerService.printStackTraceToErrorLog(request.getUsername(), e.getClass().getName(), e);

            throw e;

        } catch (ServiceException e) {
            loggerService.printStackTraceToErrorLog(request.getUsername(), e.getClass().getName(), e);

            throw e;

        } catch (Exception e) {
            loggerService.printStackTraceToErrorLog(null, e.getClass().getName(), e);

            throw new ServiceException(ExceptionConstant.SERVICE_EXCEPTION);

        }

        response.setStatus(CommonConstant.RESPONSE_SUCCESS);
        response.setStatusCode(CommonConstant.RESPONSE_CODE_SUCCESS);
        response.setMessage(CommonConstant.RESPONSE_MESSAGE_SUCCESS_EN);
        response.setTimestamp(DateUtil.formatDatetoString(currentDate, DateTimeConstant.APPLICATION_DATE_TIME_GLOBAL, null));

        return response;
    }


    private void setLoginDetailResponse(LoginResponse loginResponse, Users user, Date currentDate) throws Exception {

        UUID uuid = UUID.randomUUID();
        String generatedSID = uuid.toString();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentDate);
        calendar.add(Calendar.MINUTE, 15);

        UsersSession userSession = new UsersSession();
        userSession.setUssSid(generatedSID);
        userSession.setUssUsrId(user.getUsrId());
        userSession.setUssUsrUsername(user.getUsrUsername());
        userSession.setUssExpiryTime(calendar.getTime());
        userSession.setUssLoginDateTime(currentDate);
        userSession.setUssCreatorId(user.getUsrId());
        userSession.setUssIsDelete(CommonConstant.FLAG_N);
        userSessionDao.insert(userSession);

        loginResponse.setSid(userSession.getUssSid());

        loggerService.systemLogger(user.getUsrUsername(), "Insert authentication success. SID : " + userSession.getUssSid(), null, CommonConstant.LOG_LEVEL_INFO);

    }
    
    private void clearUserSession(String sid, Users user, Timestamp currentDate) throws Exception {
    	
    	UsersSession userSession = new UsersSession();
        userSession.setUssSid(sid);
        userSession.setUssIsDelete(CommonConstant.FLAG_N);

        UsersSession usersSession = userSessionDao.find(userSession);

        if (usersSession != null) {
        	
            userSession = usersSession;
            userSession.setUssLogoutDateTime(currentDate);
            userSession.setUssUpdaterId(user.getUsrId());
            userSession.setUssUpdateDate(currentDate);
            userSession.setUssIsDelete(CommonConstant.FLAG_Y);
            userSessionDao.update(userSession);
            loggerService.systemLogger(sid, "Clear UserSession Success.", null, CommonConstant.LOG_LEVEL_INFO);

        }
    }

}
