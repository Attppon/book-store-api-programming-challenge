package com.service.api.service;

import com.service.api.model.rest.BaseRequest;
import com.service.api.model.rest.BaseResponse;
import com.service.api.model.rest.request.OrderRequest;
import com.service.api.model.rest.request.UserRequest;
import com.service.api.model.rest.response.OrderResponse;
import com.service.api.model.rest.response.UserResponse;

public interface UserManagementService {

    public UserResponse getUser(BaseRequest request) throws Exception;
    public BaseResponse addUser(UserRequest request) throws Exception;
    public BaseResponse deleteUserHistory(BaseRequest request) throws Exception;
    public OrderResponse userOrder(BaseRequest requestHeader, OrderRequest request) throws Exception;

}
