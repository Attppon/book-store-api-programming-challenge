package com.service.api.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.service.api.constant.CommonConstant;
import com.service.api.constant.DateTimeConstant;
import com.service.api.constant.ExceptionConstant;
import com.service.api.dao.OrdersDao;
import com.service.api.dao.UserSessionDao;
import com.service.api.dao.UsersDao;
import com.service.api.domain.Orders;
import com.service.api.domain.Users;
import com.service.api.exceptions.ServiceException;
import com.service.api.model.rest.BaseRequest;
import com.service.api.model.rest.BaseResponse;
import com.service.api.model.rest.request.OrderRequest;
import com.service.api.model.rest.request.UserRequest;
import com.service.api.model.rest.response.OrderResponse;
import com.service.api.model.rest.response.UserResponse;
import com.service.api.service.ExternalService;
import com.service.api.service.LoggerService;
import com.service.api.service.UserManagementService;
import com.service.api.util.DateUtil;
import com.service.api.vo.Books;

@Service
public class UserManagementServiceImpl implements UserManagementService {

	@Autowired
	private LoggerService loggerService;
	@Autowired
	private UsersDao usersDao;
	@Autowired
	private UserSessionDao userSessionDao;
	@Autowired
	private OrdersDao ordersDao;
	@Autowired
	private ExternalService externalService;
	

	@Override
	public UserResponse getUser(BaseRequest request) throws Exception {

		UserResponse response = new UserResponse();
		Date currentDate = new Date();

		try {

			Users userDetail = usersDao.findBySid(request.getSid());

			if (userDetail != null) {

				response.setName(userDetail.getUsrName());
				response.setSurname(userDetail.getUsrSurname());
				response.setDate_of_birth(userDetail.getUsrDateOfBirth()!=null
						?DateUtil.formatDatetoString(userDetail.getUsrDateOfBirth(), DateUtil.DEFAULT_DATE_FORMAT, CommonConstant.LANGUAGE_EN):"");

				List<Orders> orders = ordersDao.findByUserId(userDetail.getUsrId());
				if(!CollectionUtils.isEmpty(orders)){
					response.setBooks(orders.stream().map(i -> i.getOdBookId()).collect(Collectors.toList()));
				}

			} else {

				loggerService.systemLogger(null, "Not found user.", null, CommonConstant.LOG_LEVEL_INFO);
				throw new ServiceException("session invalid");

			}

		} catch (ServiceException e) {
			loggerService.printStackTraceToErrorLog(request.getSid(), e.getClass().getName(), e);
			throw e;
		} catch (Exception e) {
			loggerService.printStackTraceToErrorLog(null, e.getClass().getName(), e);
			throw new ServiceException(ExceptionConstant.SERVICE_EXCEPTION);
		}

		response.setStatus(CommonConstant.RESPONSE_SUCCESS);
		response.setStatusCode(CommonConstant.RESPONSE_CODE_SUCCESS);
		response.setMessage(CommonConstant.RESPONSE_MESSAGE_SUCCESS_EN);
		response.setTimestamp(DateUtil.formatDatetoString(currentDate, DateTimeConstant.APPLICATION_DATE_TIME_GLOBAL, null));

		return response;
	}


	@Override
	public BaseResponse addUser(UserRequest request) throws Exception {

		BaseResponse response = new BaseResponse();
		Date currentDate = new Date();

		try {

			Users checkDuplicate = usersDao.findByUsername(request.getUsername().trim());
			if(checkDuplicate != null){
				response.setMessage("username is duplicate.");
			}else{
				
				Users users = new Users();
				users.setUsrUsername(request.getUsername().trim());
				users.setUsrPassword(request.getPassword().trim());
				users.setUsrName(request.getName());
				users.setUsrSurname(request.getSurname());
				users.setUsrStatus(CommonConstant.FLAG_A);

				Date dateOfBirth = DateUtil.parseStringtoDate(request.getDate_of_birth(), DateUtil.DEFAULT_DATE_FORMAT, CommonConstant.LANGUAGE_EN);
				users.setUsrDateOfBirth(DateUtil.toSqlDate(dateOfBirth));

				usersDao.insert(users);
				
				response.setMessage(CommonConstant.RESPONSE_MESSAGE_SUCCESS_EN);
			}

		} catch (ServiceException e) {
			loggerService.printStackTraceToErrorLog(request.getSid(), e.getClass().getName(), e);
			throw e;
		} catch (Exception e) {
			loggerService.printStackTraceToErrorLog(null, e.getClass().getName(), e);
			throw new ServiceException(ExceptionConstant.SERVICE_EXCEPTION);
		}

		response.setStatus(CommonConstant.RESPONSE_SUCCESS);
		response.setStatusCode(CommonConstant.RESPONSE_CODE_SUCCESS);
		response.setTimestamp(DateUtil.formatDatetoString(currentDate, DateTimeConstant.APPLICATION_DATE_TIME_GLOBAL, null));

		return response;
	}


	@Override
	public BaseResponse deleteUserHistory(BaseRequest request) throws Exception {

		BaseResponse response = new BaseResponse();
		Date currentDate = new Date();

		try {

			Users userDetail = usersDao.findBySid(request.getSid());

			if (userDetail != null) {
				ordersDao.deleteByUserId(userDetail.getUsrId());
			} else {
				loggerService.systemLogger(null, "Not found user.", null, CommonConstant.LOG_LEVEL_INFO);
				throw new ServiceException("session invalid");
			}
		} catch (ServiceException e) {
			loggerService.printStackTraceToErrorLog(request.getSid(), e.getClass().getName(), e);
			throw e;
		} catch (Exception e) {
			loggerService.printStackTraceToErrorLog(null, e.getClass().getName(), e);
			throw new ServiceException(ExceptionConstant.SERVICE_EXCEPTION);
		}

		response.setStatus(CommonConstant.RESPONSE_SUCCESS);
		response.setStatusCode(CommonConstant.RESPONSE_CODE_SUCCESS);
		response.setMessage(CommonConstant.RESPONSE_MESSAGE_SUCCESS_EN);
		response.setTimestamp(DateUtil.formatDatetoString(currentDate, DateTimeConstant.APPLICATION_DATE_TIME_GLOBAL, null));

		return response;
	}


	@Override
	public OrderResponse userOrder(BaseRequest requestHeader, OrderRequest request) throws Exception {

		OrderResponse response = new OrderResponse();
		Date currentDate = new Date();

		try {

			Users userDetail = usersDao.findBySid(requestHeader.getSid());

			if (userDetail != null) {
				
				List<Books> books = externalService.getBookByInBookId(request.getOrders());
				if(!CollectionUtils.isEmpty(books)){
					
					for (Books book : books) {
						Orders order = new Orders();
						order.setOdUsrId(userDetail.getUsrId());
						order.setOdBookId(book.getId());
						order.setOdBookName(book.getName());
						order.setOdBookPrice(book.getPrice());
						order.setOdCreatorId(userDetail.getUsrId());
						order.setOdCreator(userDetail.getUsrName());
						ordersDao.insert(order);
					}
					response.setPrice(books.stream().mapToDouble(i -> i.getPrice()).sum());
				}
			} else {
				loggerService.systemLogger(null, "Not found user.", null, CommonConstant.LOG_LEVEL_INFO);
				throw new ServiceException("session invalid");
			}
		} catch (ServiceException e) {
			loggerService.printStackTraceToErrorLog(request.getSid(), e.getClass().getName(), e);
			throw e;
		} catch (Exception e) {
			loggerService.printStackTraceToErrorLog(null, e.getClass().getName(), e);
			throw new ServiceException(ExceptionConstant.SERVICE_EXCEPTION);
		}

		response.setStatus(CommonConstant.RESPONSE_SUCCESS);
		response.setStatusCode(CommonConstant.RESPONSE_CODE_SUCCESS);
		response.setMessage(CommonConstant.RESPONSE_MESSAGE_SUCCESS_EN);
		response.setTimestamp(DateUtil.formatDatetoString(currentDate, DateTimeConstant.APPLICATION_DATE_TIME_GLOBAL, null));

		return response;
	}

}
