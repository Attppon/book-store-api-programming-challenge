package com.service.api.service;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.service.api.dao.OrdersDao;
import com.service.api.dao.UsersDao;
import com.service.api.domain.Orders;
import com.service.api.domain.Users;
import com.service.api.exceptions.ServiceException;
import com.service.api.model.rest.BaseRequest;
import com.service.api.model.rest.response.UserResponse;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import java.util.ArrayList;
import java.util.List;


@SpringBootTest
@RunWith(SpringRunner.class)
public class UserServiceTest {

	@Autowired
	private UserManagementService userManagementService;
	
	@MockBean
	private UsersDao usersDao;
	@MockBean
	private OrdersDao ordersDao;
	@MockBean
	private LoggerService loggerService;
	
	@Test
	public void getUserTestSuccess() throws Exception {
		
		Users users = new Users(); 
		users.setUsrId(Long.valueOf(1));
		users.setUsrName("name1");
		
		List<Orders> orders = new ArrayList<>();
		orders.add(new Orders(Long.valueOf(1), Long.valueOf(1), Long.valueOf(1), "Books API", Double.valueOf(150.50)));
		
		
		when(usersDao.findBySid(anyString())).thenReturn(users);
		when(ordersDao.findByUserId(anyLong())).thenReturn(orders);
		
		UserResponse userResponse = userManagementService.getUser(new BaseRequest("74fe21e0-68ab-42d5-91b1-114e257e3e7a"));
		
		Assertions.assertNotNull(userResponse);
		Assertions.assertEquals("name1", userResponse.getName());
		
	}

	@Test(expected = ServiceException.class)
	public void getUserTestFail() throws Exception {
		
		when(usersDao.findBySid(anyString())).thenReturn(null);
		doNothing().when(loggerService)
			.systemLogger(any(), any(), any(), any());
		
		UserResponse userResponse = userManagementService.getUser(new BaseRequest("74fe21e0-68ab-42d5-91b1-114e257e3e7a"));
		
		
	}
	
}
