package com.service.api.dao;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.service.api.constant.CommonConstant;
import com.service.api.dao.UsersDao;
import com.service.api.domain.Users;

@SpringBootTest
@Transactional
@RunWith(SpringRunner.class)
public class UsersDaoTest {

	@Autowired
	private UsersDao usersDao;

	@Autowired
	private NamedParameterJdbcTemplate subject;

	@Before
	public void insertDataInto(){
		String sql =
				"INSERT INTO USERS(USR_CREATE_DATE, USR_NAME, USR_SURNAME,USR_DATE_OF_BIRTH, USR_USERNAME, USR_PASSWORD, USR_STATUS)" +
						"VALUES (sysdate(),'TestName2', 'TestSurname2', '1992-06-17', 'user02', 'password99', 'A')";

		subject.update(sql, Collections.emptyMap());
	}

	@Test
	public void findByUsernameSuccess() throws Exception {
		Users users = usersDao.findByUsername("user02");
		Assertions.assertNotNull(users);
	}

	@Test
	public void createUserTestSuccess() throws Exception{

		Users user = new Users();
		user.setUsrName("test");
		user.setUsrSurname("test");
		user.setUsrUsername("user01");
		user.setUsrPassword("password99");
		user.setUsrStatus(CommonConstant.FLAG_A);
		Long affectKey = usersDao.insert(user);

		Users userResult = usersDao.findByUsername("user01");

		Assertions.assertNotNull(affectKey);
		Assertions.assertNotNull(userResult);
	}


	@Test
	public void findByUsernameAndPasswordSuccess() throws Exception {

		Users users = usersDao.findByUsernameAndPassword(new Users("superadmin", "password99"));
		Assertions.assertNotNull(users);
		Assertions.assertEquals("admin", users.getUsrName());
		Assertions.assertEquals("admin", users.getUsrSurname());
	}
}
